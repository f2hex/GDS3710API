# GDS3710API - an API based command line tool for GDS3710 VoIP based intercom

This is a simple command line utility to perform some basic operation
with GDS3710.

It can be used as a standalone python 3 program or as a Docker image.
The Dockerfile included here is for use on an ARM board but it can be
easily re-adapted to any archictecture and is based on an Alpine image.

## Commands

The original intentio for this software is to provide a quick hack to
"disable" GDS3710 from a user point of view.
The program can be invoked passing one of the following commands:

* **Disable**: it executes API calls to set the 'Number Called When
  Door Bell Pressed' to a non existent numnber (in my case 800), set the
  audio 'System Volume' and set 'Dorbell Volume' to zero. In this way
  any time the bell button pressed on GDS3710 does not produce any ring
  on the VoIP devices associated with the group call number.

* **Enable**: it executes API calls to set back the 'Number Called
  When Door Bell Pressed', 'System Volume' and 'Dorbell Volume' to the
  original values.

* **Status**: print out the configuration values of the 'Number Called
  When Door Bell Pressed', 'System Volume' and 'Dorbell Volume' settings.

## To run the program as Docker container

Just execute this command to get the current status of the settings:

```
docker run -it --rm f2hex/arm64-gds3710:0.2 -u GDS3710_URL -p GDS3710_ADMIN_PASSWORD status
```


## Why

This is a temporary hack to solve the issue of the, as I call it,
_Gecko effect_ on my GDS3710. In practice, sometime in the night, a
gecko walks over the GDS3710 keypad activating the doorbell button. As
you can imagine this is quite annoying especially when this occurs at
2AM or 3AM...

As far as I know there is no way to tune the sensitivity of the keypad
doorbel button. I thought also about other ways to solve this issue
but so far I have not found anything really effective - even a
"physical" protection that does interfere with the normal GDS3710 use.

So in the end with this program, scheduled to run using `crontab`, on
one of my Linux ARM boards (that are on the same LAN as the GDS3710) I
can "silence" temporarily the GDS3710 during the night until I found a
more convenient way to solve this issue.

## Warning

Use this program at your own risk - I am not responsible for
misconfiguring your GDS3710. Moreover I have my GDS3710 completely
"air-gapped" into my LAN - in fact is not directly connected with any
external network - I use a VPN for this purpose.

## License

This is released using LGPL license/
