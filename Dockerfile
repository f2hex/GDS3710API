# ; -*- mode: dockerfile;-*-
FROM python:alpine3.19
MAINTAINER Franco Fiorese  <franco.fiorese@gmail.com>

ENV CUSER_ID=5000
ENV CUSER=cuser

# create $CUSER with no password

WORKDIR /app

RUN    apk --update add --no-cache openssl \
    && addgroup -g $CUSER_ID $CUSER \
    && adduser -u $CUSER_ID -G $CUSER -H -D $CUSER
RUN    pip install requests urllib3

COPY   gds3710.py ./gds3710.py

RUN chown -R $CUSER:$CUSER /app
USER cuser


ENTRYPOINT ["python", "/app/gds3710.py" ]
