#!/usr/bin/env python3

"""
 GDS3710API

 Very basic class for dealing with Grandstream GDS3710 IP intercom.

 Date: 16-Aug-2019

 Copyright (C) 2019 Franco Fiorese <franco.fiorese@gmail.com>

 Main module

"""
VERSION=0.1

import requests, argparse, sys
import http.client, ssl, hashlib
import pprint

import xml.etree.ElementTree as ET
import urllib3


urllib3.disable_warnings()
#import logging

#logging.basicConfig(level=logging.DEBUG)

class AuthenticationErrorException(Exception):
    def __init__(self, message, error):
        super().__init__(message)
        self.error = error

class ConfigGetErrorException(Exception):
    def __init__(self, message, error):
        super().__init__(message)
        self.error = error

class UnavailableConfigDataException(Exception):
    def __init__(self, message, error):
        super().__init__(message)
        self.error = error

class ConfigSetErrorException(Exception):
    def __init__(self, message, error):
        super().__init__(message)
        self.error = error

class GDS3710API():
    def __init__(self, url, passwd, ssl_verify=True):
        self.url = url
        self.session = None
        self.verify = ssl_verify
        self.passwd = passwd
        self.ccode0 = "GDS3710lZpRsFzCbM"
        self.ccode1 = "GDS3710lDyTlHwNgZ"

    def _authenticate(self, mtype):
        if mtype == "1":
            gds_intpwd = self.ccode1
        else:
            gds_intpwd = self.ccode0
            res = requests.get("{0}/goform/login?cmd=login&user=admin&type={1}".format(self.url, mtype), verify=self.verify)
            if res.status_code == 200:
                root = ET.fromstring(res.text)
                ccode = root.find("ChallengeCode").text
                hl = hashlib.md5()
                msg = "{0}:{1}:{2}".format(ccode, gds_intpwd, self.passwd).encode('utf-8')
                hl.update(msg)
                authcode = hl.hexdigest()
                res = requests.get("{0}/goform/login?cmd=login&user=admin&authcode={1}&type={2}".format(self.url, authcode, mtype), verify=self.verify)
                if res.status_code == 200:
                    xml = ET.fromstring(res.text)
                    rc = int(xml.find("ResCode").text)
                    if rc != 0:
                        raise AuthenticationErrorException("Authentication error", res.status_code)
                    else:
                        self.session = res.cookies
                else:
                    raise AuthenticationErrorException("Authentication error", res.status_code)
            else:
                raise AuthenticationErrorException("Authentication error", res.status_code)


    def get_snapshot(self, img_destfile):
        self._authenticate("1")
        if self.session:
            res = requests.get("{0}/snapshot/view.jpeg".format(self.url), verify=self.verify, cookies=self.session, stream=True)
            if res.status_code == 200:
                img = res.raw.read()
                with open(img_destfile, 'wb') as f:
                    f.write(img)

    def get_config(self, mtype):
        cfg = {}
        self._authenticate(mtype)
        if self.session:
            res = requests.get("{0}/goform/config?cmd=get&type={1}".format(self.url, mtype), verify=self.verify,
                               cookies=self.session, stream=True)
            if res.status_code == 200:
                xml = ET.fromstring(res.text)
                rc = xml.find("ResCode").text
                if rc == "0":
                    for tags in xml.iter('Configuration'):
                        for child in tags:
                            if child != "RetMsg" and child != "ResCod":
                                cfg[child.tag] = child.text
                else:
                    raise UnavailableConfigDataException("No available config data", rc)
            else:
                raise ConfigGetErrorException("Cannot retrieve configuration data", res.status_code)

        return cfg

    def  set_config(self, mtype, param, value):
        self._authenticate(mtype)
        if self.session:
            res = requests.get("{0}/goform/config?cmd=set&type={1}&{2}={3}".format(self.url, mtype, param, value),
                               verify=self.verify, cookies=self.session, stream=True)
            if res.status_code == 200:
                return(res.text)
            else:
                raise ConfigSetErrorException("Cannot set configuration data", res.status_code)
        return ""




def main(args):
    """
    Entry point
    """
    # Parameter check and parsing
    parser = argparse.ArgumentParser(description="GDS3710 API CLI", epilog="Execute GDS3710get/set command using API")
    parser.add_argument("-v", "--version", action="version", version="%(prog)s {0}".format(VERSION))
    parser.add_argument("-u", action="store", dest="url", help="the GDS3710 device URL", required = True)
    parser.add_argument("-p", action="store", dest="passwd", help="GDS3710 admin password", required = True)
    parser.add_argument("cmd", metavar="COMMAND", type=str, help="command: can be 'enable', 'disable' or 'status'")

    args = parser.parse_args()
    print("Executing {0} command".format(args.cmd))
    rc = 0
    if args.cmd:
        if args.cmd == "enable":
            gds = GDS3710API(args.url, args.passwd, ssl_verify=False)
            gds.set_config("door", "P10462", "599")
            gds.set_config("audio", "P14003", 5)
            gds.set_config("audio", "P14835", 1)

        elif args.cmd == "disable":
            gds = GDS3710API(args.url, args.passwd, ssl_verify=False)
            gds.set_config("door", "P10462", "800")
            gds.set_config("audio", "P14003", 0)
            gds.set_config("audio", "P14835", 0)
        elif args.cmd == "status":
            gds = GDS3710API(args.url, args.passwd, ssl_verify=False)
            cfg = gds.get_config("door")
            print("door module:\n {0}".format(cfg))
            cfg = gds.get_config("audio")
            print("audio module:\n {0}".format(cfg))
        else:
            print("invalid command")
            rc = -2

    else:
        print("command is missing")
        rc = -1

    return rc


if __name__ == "__main__":
    sys.exit(main(sys.argv))
