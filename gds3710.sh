#!/bin/sh
PATH=/usr/local/bin:/usr/local/sbin:~/bin:/usr/bin:/bin:/usr/sbin:/sbin
CIMAGE="git.f2hex.net/f2hex/gds3710api:1.1"
LOGFILE="/var/log/gds3710.log"
TS=`date`
echo "$TS: $1" >> $LOGFILE 
docker run --rm "${CIMAGE}" -u http://polifemo.olimpo.lan -p PASSWORD $1
docker run --rm "${CIMAGE}" -u http://polifemo.olimpo.lan -p PASSWORD status >> $LOGFILE

